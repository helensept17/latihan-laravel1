<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction_details extends Model
{
    use HasFactory;
     /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'qty', 'price'
    ];
/**
 * Transactions
 * 
 * @return void
 */
public function Transactions()
{
    return $this->belongsTo(Transactions::class);
}

/**
 * products
 * 
 * @return void
 */
public function products()
{
    return $this->belongsTo(products::class);
}
}
