<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Castss\Attribute;

class products extends Model
{
    use HasFactory;
     /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'image', 'barcode', 'title', 'description', 'buy_price', 'sell_price', 'stock'
    ];
/**
 * categories
 * 
 * @return void
 */
public function categories()
{
    return $this->belongsTo(categories::class);
}

/**
 * Transactions_details
 * 
 * @return void
 */
public function Transactions_details()
{
    return $this->hasMany(Transactions_details::class);
}

/**
 * image
 * 
 * @return attribute
 */
protected function image(): Attribute
{
    return Attribute::make(
        get: fn ($value) => asset('/storage/products/' . $value),
    );
}
}
