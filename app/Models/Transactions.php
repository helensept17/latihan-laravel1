<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attibute;


class transactions extends Model
{
    use HasFactory;
     /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'invoice', 'cash', 'change', 'discount', 'grand_total'
    ];
/**
 * Transactions_details
 * 
 * @return void
 */
public function Transactions_details()
{
    return $this->hasMany(Transaction_details::class);
}

/**
 * customer
 * 
 * @return void
 */
public function customer()
{
    return $this->belongsTo(customer::class);
}

/**
 * cashier
 * 
 * @return void
 */
public function cashier()
{
    return $this->belongsTo(users::class, 'cashier_id');
}

/**
 * profits
 * 
 * @return void
 */
public function profits()
{
    return $this->hasMany(profits::class);
}

/**
 * createdAt
 * 
 * @return attribute
 */
protected function createdAt(): Attribute
{
    return Attribute::make(
        get: fn ($value) => Carbon::parse($value)->format('d-M-Y H:i:s'),
    );
}
}
