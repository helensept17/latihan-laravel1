<?php

namespace Database\Seeders;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create user
        $user = user::create([
            'name' => 'administrator',
            'email' => 'admin@gmail.com',
            'password' => 'bcrypt'('password'),
        ]);

        //getall permission
        $permission = Permissison::all();

        //get role admin
        $role = Role::find(1);

        //assign permission to role
        $role->syncPermission($permission);

        //assign role to user
        $user->assignRole($role);
    }
}
